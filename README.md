# Movie Recommendations System

## Description

This basic API that allows users to rank movies they have watched and received suggestions for other movies to watch.

After a user has rated at least 3 movies, his rated movies are compared to 10 other ramdom users and suggestions are generated based on their similaries and the movies that the user has not seen.

The user will not receive a recommendation for a movie that he has already watched and can update the rank of a movie he already rated by just ranking it again.

After suggestions for movies are generated, you will also get a list user profiles with a likeness score to the user. A likeness score of 10 is most similar to the user in taste and a 1 is the lowest.

Similar profiles, can provide insight on a buddy that will be a good campanion to watch movies with. 

The random element is intented to provide diversity and avoid getting bad suggestions like on Netflix. While also avoiding movies that you also watched. 

The system has not authentication and should be implemented to prevent the corruption of data and or improve security.

## Getting started

npm install
npm start 

## Future Integrations

- Use API from https://www.omdbapi.com/ to get all movie info.
- Social Media Auth - FB

## Test Functions

npm test 

*There is a slight bug that the test may fail with:

 console.error
    Error: SyntaxError: Unexpected end of JSON input
        at JSON.parse (<anonymous>)
        at loadMovies (/src/data-handler.ts:9:15)
        at createMovie (/src/create-movie.ts:7:25)

But if you run it again it will most likely pass:

 PASS  src/__tests__/app.test.ts
  API Movie Tests
    ✓ check if the movie exists (55 ms)
    ✓ should create a new movie successfully (30 ms)
    ✓ should rank a movie and update user ratings (17 ms)
    ✓ should suggest movies (17 ms)
    ✓ should suggest similar profiles (12 ms)

Test Suites: 1 passed, 1 total
Tests:       5 passed, 5 total
Snapshots:   0 total
Time:        1.467 s, estimated 2 s
Ran all test suites.


## Usage

// POST /api/rank

Allows you to rank and movie. 
The function will also create a user profile if it doesn't exist and or update the movie rating for that user.
*You can only rate a movie by "imdb_id" and only if its been added to the ./data/movies/movies.json.

RAW JSON SAMPLE:

{
    "user": "sempervalor",
    "imdb_id": "tt0110912",
    "rating": 7
}

RETURNS: the user's entire profile.

// POST /api/create-movie 

Allows you to add movies to the db.
*At least send imdb_id and title must be sent in the JSON, the rest of the meta data can be received via api using a micro service that might be implemented in the future.

RAW JSON SAMPLE:

{
    "imdb_id": "tt1375666",
    "title": "Inception",
    "year": 2010,
    "genre": ["Action", "Adventure", "Sci-Fi"],
    "director": "Christopher Nolan",
    "writers": ["Christopher Nolan"],
    "actors": ["Leonardo DiCaprio", "Joseph Gordon-Levitt", "Elliot Page"],
    "plot": "A thief who enters the dreams of others to steal their secrets.",
    "rating": 8.8,
    "awards": ["4 Oscars", "Critical acclaim"],
    "runtime": 148,
    "country": "USA",
    "language": "English"
}

RETURNS: either that the movie already exists or it was created.

// GET /api/suggest-movies/?u=FacebookUsername

RETURNS: movie suggestions along with similar profiles. It will only return suggestions if the user has rated 3 movies.

// GET /api/suggest-similar-profiles/?u=FacebookUsername 
*I got lazy and used the same function since it also returns similar profiles and there was no need to run the function again.
This can be expended in the future to create period vertices for suggestions and more complex edges.

## Data Storage Logic

Movies are keep in ./data/movies/movies.json 
    - All movies in have a unique "imdb_id" and no duplicates should be allowed.
    - For scalabilty, stored movie data can later be broken down into genre subdirectories using any graph data model.

Users are kept in ./data/users/${user}.json 
    - Users only have 1 unique json profile file. * No duplicates are allowed.
    - For scalabilty, stored user data can later be broken down into subdirectories using any graph data model. This can help create different vertices to provide user insights. For example, period based cached suggested etc...

*Its important to in the future limit the JSOn file size not to exceed 2mb. This is a keyfactor in breaking the files down in the future to create a pseudo distributive system.

## Authors and acknowledgment
Maikel Habib

## License
TDB

## Project status
Beta

import express, { Request, Response } from 'express';
import bodyParser from 'body-parser';
import { suggestMovies } from './suggestions';
import { createMovie } from './create-movie';
import { rankMovie } from './rank';

const app = express();
const port = 3000;

app.use(bodyParser.json());

// POST /api/create-movie 
// *At least send imdb_id and title, the rest of the meta data can be received via api using a micro service
app.post('/api/create-movie', createMovie);

// POST /api/rank
app.post('/api/rank', rankMovie);

// GET /api/suggest-movies/?u=FacebookUsername
app.get('/api/suggest-movies/', suggestMovies);

// GET /api/suggest-similar-profiles/?u=FacebookUsername *got lazy and used the same function since it also returns similar profiles
app.get('/api/suggest-similar-profiles/', suggestMovies);

const server = app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});

export { app, server };

import { Request, Response } from 'express';
import { loadMovies, moviesFilePath, loadUser, saveUser, usersDirPath } from './data-handler';

export const rankMovie = async (req: Request, res: Response) => {
  try {
    const { user, imdb_id, rating } = req.body;

    // Step 1: Check if the movie exists in movies.json
    const moviesArray = await loadMovies(moviesFilePath);
    const movie = moviesArray.find((movie: { imdb_id: any; }) => movie.imdb_id === imdb_id);

    if (!movie) {
      res.status(404).json({ message: 'Movie not found' });
      return;
    }

    // Step 2: Check if the user file exists in data/users/
    const userFilePath = `${usersDirPath}${user}.json`;
    let userData = await loadUser(userFilePath);

    console.log(userFilePath);

    if (!userData) {
      // Step 3: If the user file doesn't exist, create a new one
      userData = { user, ratedMovies: [] };
    }

    // Step 4: Check if the user has already rated the movie
    const existingRatingIndex = userData.ratedMovies.findIndex((movie: { imdb_id: any; }) => movie.imdb_id === imdb_id);

    if (existingRatingIndex !== -1) {
      // Step 5: If the user has already rated the movie, update the rating and timestamp
      userData.ratedMovies[existingRatingIndex] = { imdb_id, rating, timestamp: new Date().toISOString() };
    } else {
      // If the user has not rated the movie, append the new rating with timestamp
      userData.ratedMovies.push({ imdb_id, rating, timestamp: new Date().toISOString() });
    }

    // Step 6: Save the updated/created user file
    await saveUser(userFilePath, userData);

    // Return the updated user data
    res.status(200).json(userData);
  } catch (error: any) {
    console.error('Error:', error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

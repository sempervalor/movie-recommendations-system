import * as fs from 'fs/promises';

export const moviesFilePath = 'src/data/movies/movies.json'; 
export const usersDirPath = 'src/data/users/'; 

// Load movies from the JSON file
export async function loadMovies(filePath: string): Promise<any[]> {
  const data = await fs.readFile(filePath, 'utf-8');
  return JSON.parse(data);
}

// Save movies to the JSON file
export async function saveMovies(filePath: string, moviesArray: any[]): Promise<void> {
  await fs.writeFile(filePath, JSON.stringify(moviesArray, null, 2), 'utf-8');
}

export const removeMovieByImdbId = async (imdbIdToRemove: any) => {
  // Read the content of the movies file
  const moviesContent = fs.readFile(moviesFilePath, 'utf8');

  // Parse the JSON content into an array
  const movies = JSON.parse(await moviesContent);

  // Find the index of the movie with the specified imdb_id
  const indexToRemove = movies.findIndex((movie: { imdb_id: any; }) => movie.imdb_id === imdbIdToRemove);

  // If the movie was found, remove it from the array
  if (indexToRemove !== -1) {
    movies.splice(indexToRemove, 1);

    // Save the updated movies array back to the file
    fs.writeFile(moviesFilePath, JSON.stringify(movies, null, 2));

    console.log(`Movie with imdb_id ${imdbIdToRemove} removed successfully.`);
  } else {
    console.log(`Movie with imdb_id ${imdbIdToRemove} not found.`);
  }
};

// Load User Data

export async function loadUser(filePath: string): Promise<any | null> {
  try {
    const data = await fs.readFile(filePath, 'utf-8');
    return JSON.parse(data);
  } catch (error) {
    // Handle the case when the file doesn't exist
    return null;
  }
}

// Save User Data
export async function saveUser(filePath: string, userData: any): Promise<void> {
  await fs.writeFile(filePath, JSON.stringify(userData, null, 2), 'utf-8');
}

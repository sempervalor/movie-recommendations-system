import { Request, Response } from 'express';
import { loadMovies, saveMovies, moviesFilePath } from './data-handler';

export const createMovie = async (req: Request, res: Response) => {
  try {
    const movieData = req.body;
    const moviesArray = await loadMovies(moviesFilePath);

    // Step 1: Check if movie with the provided imdb_id already exists
    const existingMovie = moviesArray.find((movie: { imdb_id: any; }) => movie.imdb_id === movieData.imdb_id);

    if (existingMovie) {
      // Movie already exists, return a message with details
      res.status(400).json({ message: 'Movie already exists', existingMovie });
    } else {
      // Step 2: Movie does not exist, add it to the array
      moviesArray.push(movieData);
      await saveMovies(moviesFilePath, moviesArray);

      // Step 3: Return the final JSON response
      res.status(201).json({ message: 'Movie created successfully', movieData });
    }
  } catch (error) {
    console.error('Error:', error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

import { Request, Response } from 'express';
import * as fs from 'fs/promises';
import { loadUser, usersDirPath } from './data-handler';

export const suggestMovies = async (req: Request, res: Response) => {
  try {
    const { u: user } = req.query;

    // Step 1: Retrieve the user.json file for the given user and check if he has rated at least 3 movies
    const userFilePath = `${usersDirPath}${user}.json`;
    const userData = await loadUser(userFilePath);

    // Make sure the user exists
    if (!userData) {
      res.status(404).json({ message: 'User not found' });
      return;
    }

    // Require users to have rated more than 3 movies
    if (userData.ratedMovies.length < 3) {
      res.status(400).json({ message: 'User has not rated enough movies for suggestions' });
      return;
    }

    // Step 2: Get a max of 10 random user.json files from /data/users/
    const allUsersFiles = await fs.readdir(`${usersDirPath}/`);
    const randomUserFiles = allUsersFiles
      .filter(filename => filename.endsWith('.json') && filename !== `${user}.json`)
      .sort(() => Math.random() - 0.5)
      .slice(0, 10);

    // Step 3: Place each file retrieved for the 10 random files within an array
    const userProfiles = await Promise.all(
      randomUserFiles.map(async filename => {
        const userProfile = await loadUser(`src/data/users/${filename}`);
        return userProfile;
      })
    );

    // Step 4: Get all imdb_ids for the given user
    const userImdbIds = userData.ratedMovies.map((movie: { imdb_id: string }) => movie.imdb_id);

    // Step 5: Filter out users without at least 1 matching imdb_id in their ratedMovies
    const filteredUserProfiles = userProfiles.filter(profile =>
      profile.ratedMovies.some((movie: { imdb_id: string }) => userImdbIds.includes(movie.imdb_id))
    );

    // Step 6: Calculate likeness values for each user top the given user
    const likenessValues = filteredUserProfiles.map(profile => {
      const commonImdbIds = profile.ratedMovies.filter((movie: { imdb_id: string }) =>
        userImdbIds.includes(movie.imdb_id)
      );
      const likenessScore = commonImdbIds.reduce((score: number, movie: { rating: number }) => score + movie.rating, 0) / commonImdbIds.length;
      return { user: profile.user, likeness: Math.round(likenessScore) };
    });

    // Step 7: Remove imdb_ids for the given user from the userProfiles
    userProfiles.forEach(profile => {
      profile.ratedMovies = profile.ratedMovies.filter((movie: { imdb_id: string }) => !userImdbIds.includes(movie.imdb_id));
    });

    // Step 8: Sort ratedMovies from highest to lowest ranking and limit to 10 suggestions
    const suggestions = userData.ratedMovies.sort((a: { rating: number }, b: { rating: number }) => b.rating - a.rating).slice(0, 10);

    // Step 9: Remove duplicate imdb_ids from suggestions
    const uniqueSuggestions = suggestions.filter((value: { imdb_id: string; }, index: any, self: { imdb_id: string; }[]) => self.findIndex((s: { imdb_id: string }) => s.imdb_id === value.imdb_id) === index);

    // Step 10: Return the final JSON response
    res.status(200).json({
      user,
      suggestions: uniqueSuggestions,
      similarProfiles: likenessValues,
    });
  } catch (error: any) {
    console.error('Error:', error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

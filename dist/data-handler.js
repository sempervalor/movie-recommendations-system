"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.saveUser = exports.loadUser = exports.removeMovieByImdbId = exports.saveMovies = exports.loadMovies = exports.usersDirPath = exports.moviesFilePath = void 0;
const fs = __importStar(require("fs/promises"));
exports.moviesFilePath = 'src/data/movies/movies.json';
exports.usersDirPath = 'src/data/users/';
// Load movies from the JSON file
async function loadMovies(filePath) {
    const data = await fs.readFile(filePath, 'utf-8');
    return JSON.parse(data);
}
exports.loadMovies = loadMovies;
// Save movies to the JSON file
async function saveMovies(filePath, moviesArray) {
    await fs.writeFile(filePath, JSON.stringify(moviesArray, null, 2), 'utf-8');
}
exports.saveMovies = saveMovies;
const removeMovieByImdbId = async (imdbIdToRemove) => {
    // Read the content of the movies file
    const moviesContent = fs.readFile(exports.moviesFilePath, 'utf8');
    // Parse the JSON content into an array
    const movies = JSON.parse(await moviesContent);
    // Find the index of the movie with the specified imdb_id
    const indexToRemove = movies.findIndex((movie) => movie.imdb_id === imdbIdToRemove);
    // If the movie was found, remove it from the array
    if (indexToRemove !== -1) {
        movies.splice(indexToRemove, 1);
        // Save the updated movies array back to the file
        fs.writeFile(exports.moviesFilePath, JSON.stringify(movies, null, 2));
        console.log(`Movie with imdb_id ${imdbIdToRemove} removed successfully.`);
    }
    else {
        console.log(`Movie with imdb_id ${imdbIdToRemove} not found.`);
    }
};
exports.removeMovieByImdbId = removeMovieByImdbId;
// Load User Data
async function loadUser(filePath) {
    try {
        const data = await fs.readFile(filePath, 'utf-8');
        return JSON.parse(data);
    }
    catch (error) {
        // Handle the case when the file doesn't exist
        return null;
    }
}
exports.loadUser = loadUser;
// Save User Data
async function saveUser(filePath, userData) {
    await fs.writeFile(filePath, JSON.stringify(userData, null, 2), 'utf-8');
}
exports.saveUser = saveUser;

"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.suggestMovies = void 0;
const fs = __importStar(require("fs/promises"));
const data_handler_1 = require("./data-handler");
const suggestMovies = async (req, res) => {
    try {
        const { u: user } = req.query;
        // Step 1: Retrieve the user.json file for the given user and check if he has rated at least 3 movies
        const userFilePath = `${data_handler_1.usersDirPath}${user}.json`;
        const userData = await (0, data_handler_1.loadUser)(userFilePath);
        // Make sure the user exists
        if (!userData) {
            res.status(404).json({ message: 'User not found' });
            return;
        }
        // Require users to have rated more than 3 movies
        if (userData.ratedMovies.length < 3) {
            res.status(400).json({ message: 'User has not rated enough movies for suggestions' });
            return;
        }
        // Step 2: Get a max of 10 random user.json files from /data/users/
        const allUsersFiles = await fs.readdir(`${data_handler_1.usersDirPath}/`);
        const randomUserFiles = allUsersFiles
            .filter(filename => filename.endsWith('.json') && filename !== `${user}.json`)
            .sort(() => Math.random() - 0.5)
            .slice(0, 10);
        // Step 3: Place each file retrieved for the 10 random files within an array
        const userProfiles = await Promise.all(randomUserFiles.map(async (filename) => {
            const userProfile = await (0, data_handler_1.loadUser)(`src/data/users/${filename}`);
            return userProfile;
        }));
        // Step 4: Get all imdb_ids for the given user
        const userImdbIds = userData.ratedMovies.map((movie) => movie.imdb_id);
        // Step 5: Filter out users without at least 1 matching imdb_id in their ratedMovies
        const filteredUserProfiles = userProfiles.filter(profile => profile.ratedMovies.some((movie) => userImdbIds.includes(movie.imdb_id)));
        // Step 6: Calculate likeness values for each user top the given user
        const likenessValues = filteredUserProfiles.map(profile => {
            const commonImdbIds = profile.ratedMovies.filter((movie) => userImdbIds.includes(movie.imdb_id));
            const likenessScore = commonImdbIds.reduce((score, movie) => score + movie.rating, 0) / commonImdbIds.length;
            return { user: profile.user, likeness: Math.round(likenessScore) };
        });
        // Step 7: Remove imdb_ids for the given user from the userProfiles
        userProfiles.forEach(profile => {
            profile.ratedMovies = profile.ratedMovies.filter((movie) => !userImdbIds.includes(movie.imdb_id));
        });
        // Step 8: Sort ratedMovies from highest to lowest ranking and limit to 10 suggestions
        const suggestions = userData.ratedMovies.sort((a, b) => b.rating - a.rating).slice(0, 10);
        // Step 9: Remove duplicate imdb_ids from suggestions
        const uniqueSuggestions = suggestions.filter((value, index, self) => self.findIndex((s) => s.imdb_id === value.imdb_id) === index);
        // Step 10: Return the final JSON response
        res.status(200).json({
            user,
            suggestions: uniqueSuggestions,
            similarProfiles: likenessValues,
        });
    }
    catch (error) {
        console.error('Error:', error);
        res.status(500).json({ message: 'Internal Server Error' });
    }
};
exports.suggestMovies = suggestMovies;

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.server = exports.app = void 0;
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const suggestions_1 = require("./suggestions");
const create_movie_1 = require("./create-movie");
const rank_1 = require("./rank");
const app = (0, express_1.default)();
exports.app = app;
const port = 3000;
app.use(body_parser_1.default.json());
// POST /api/create-movie 
// *At least send imdb_id and title, the rest of the meta data can be received via api using a micro service
app.post('/api/create-movie', create_movie_1.createMovie);
// POST /api/rank
app.post('/api/rank', rank_1.rankMovie);
// GET /api/suggest-movies/?u=FacebookUsername
app.get('/api/suggest-movies/', suggestions_1.suggestMovies);
// GET /api/suggest-similar-profiles/?u=FacebookUsername *got lazy and used the same function since it also returns similar profiles
app.get('/api/suggest-similar-profiles/', suggestions_1.suggestMovies);
const server = app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});
exports.server = server;

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const data_handler_1 = require("@src/data-handler");
const supertest_1 = __importDefault(require("supertest"));
const { app, server } = require('@src/app');
describe('API Movie Tests', () => {
    afterAll((done) => {
        server.close(done);
    });
    it('check if the movie exists', async () => {
        const existingMovieData = {
            "imdb_id": "tt1375666",
            "title": "Inception",
            "year": 2010,
            "genre": ["Action", "Adventure", "Sci-Fi"],
            "director": "Christopher Nolan",
            "writers": ["Christopher Nolan"],
            "actors": ["Leonardo DiCaprio", "Joseph Gordon-Levitt", "Elliot Page"],
            "plot": "A thief who enters the dreams of others to steal their secrets.",
            "rating": 8.8,
            "awards": ["4 Oscars", "Critical acclaim"],
            "runtime": 148,
            "country": "USA",
            "language": "English"
        };
        // Mock the loadMovies function to return the existing movie data
        jest.mock('@src/data-handler', () => ({
            loadMovies: jest.fn().mockResolvedValue([existingMovieData]),
            saveMovies: jest.fn()
        }));
        // Import your app after mocking
        const { app } = require('@src/app');
        const response = await (0, supertest_1.default)(app).post('/api/create-movie');
        expect(response.status).toBe(400);
        expect(response.body).toEqual({
            message: 'Movie already exists',
            existingMovie: {},
        });
    });
    it('should create a new movie successfully', async () => {
        const movieData = {
            "imdb_id": "tt000",
            "title": "Fake Movie",
            "year": 2010,
            "genre": ["Action", "Adventure", "Sci-Fi"],
            "director": "Christopher Nolan",
            "writers": ["Christopher Nolan"],
            "actors": ["Leonardo DiCaprio", "Joseph Gordon-Levitt", "Elliot Page"],
            "plot": "A thief who enters the dreams of others to steal their secrets.",
            "rating": 8.8,
            "awards": ["4 Oscars", "Critical acclaim"],
            "runtime": 148,
            "country": "USA",
            "language": "English"
        };
        (0, data_handler_1.removeMovieByImdbId)(movieData.imdb_id);
        jest.mock('@src/data-handler', () => ({
            loadMovies: jest.fn().mockResolvedValue([]),
            saveMovies: jest.fn(),
        }));
        // Import your app after mocking
        const { app } = require('@src/app');
        // Make the API request
        const response = await (0, supertest_1.default)(app)
            .post('/api/create-movie')
            .send(movieData)
            .set('Accept', 'application/json');
        // Perform assertions
        expect(response.status).toBe(201);
        expect(response.body.message).toEqual('Movie created successfully');
    });
    it('should rank a movie and update user ratings', async () => {
        const requestBody = {
            "user": "sempervalor",
            "imdb_id": "tt0111161",
            "rating": 3
        };
        const response = await (0, supertest_1.default)(app)
            .post('/api/rank')
            .send(requestBody)
            .set('Accept', 'application/json');
        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty('user', 'sempervalor');
        expect(response.body.ratedMovies).toEqual(expect.arrayContaining([
            expect.objectContaining({
                imdb_id: 'tt0111161',
                rating: 3,
                timestamp: expect.any(String)
            })
        ]));
    });
    it('should suggest movies', async () => {
        const response = await (0, supertest_1.default)(app).get('/api/suggest-movies/?u=sempervalor');
        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty('user', 'sempervalor');
        expect(response.body).toHaveProperty('suggestions');
        expect(response.body).toHaveProperty('similarProfiles');
    });
    it('should suggest similar profiles', async () => {
        const response = await (0, supertest_1.default)(app).get('/api/suggest-similar-profiles/?u=sempervalor');
        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty('user', 'sempervalor');
        expect(response.body).toHaveProperty('suggestions');
        expect(response.body).toHaveProperty('similarProfiles');
    });
});

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.rankMovie = void 0;
const data_handler_1 = require("./data-handler");
const rankMovie = async (req, res) => {
    try {
        const { user, imdb_id, rating } = req.body;
        // Step 1: Check if the movie exists in movies.json
        const moviesArray = await (0, data_handler_1.loadMovies)(data_handler_1.moviesFilePath);
        const movie = moviesArray.find((movie) => movie.imdb_id === imdb_id);
        if (!movie) {
            res.status(404).json({ message: 'Movie not found' });
            return;
        }
        // Step 2: Check if the user file exists in data/users/
        const userFilePath = `${data_handler_1.usersDirPath}${user}.json`;
        let userData = await (0, data_handler_1.loadUser)(userFilePath);
        console.log(userFilePath);
        if (!userData) {
            // Step 3: If the user file doesn't exist, create a new one
            userData = { user, ratedMovies: [] };
        }
        // Step 4: Check if the user has already rated the movie
        const existingRatingIndex = userData.ratedMovies.findIndex((movie) => movie.imdb_id === imdb_id);
        if (existingRatingIndex !== -1) {
            // Step 5: If the user has already rated the movie, update the rating and timestamp
            userData.ratedMovies[existingRatingIndex] = { imdb_id, rating, timestamp: new Date().toISOString() };
        }
        else {
            // If the user has not rated the movie, append the new rating with timestamp
            userData.ratedMovies.push({ imdb_id, rating, timestamp: new Date().toISOString() });
        }
        // Step 6: Save the updated/created user file
        await (0, data_handler_1.saveUser)(userFilePath, userData);
        // Return the updated user data
        res.status(200).json(userData);
    }
    catch (error) {
        console.error('Error:', error);
        res.status(500).json({ message: 'Internal Server Error' });
    }
};
exports.rankMovie = rankMovie;

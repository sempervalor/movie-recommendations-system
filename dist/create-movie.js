"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createMovie = void 0;
const data_handler_1 = require("./data-handler");
const createMovie = async (req, res) => {
    try {
        const movieData = req.body;
        const moviesArray = await (0, data_handler_1.loadMovies)(data_handler_1.moviesFilePath);
        // Step 1: Check if movie with the provided imdb_id already exists
        const existingMovie = moviesArray.find((movie) => movie.imdb_id === movieData.imdb_id);
        if (existingMovie) {
            // Movie already exists, return a message with details
            res.status(400).json({ message: 'Movie already exists', existingMovie });
        }
        else {
            // Step 2: Movie does not exist, add it to the array
            moviesArray.push(movieData);
            await (0, data_handler_1.saveMovies)(data_handler_1.moviesFilePath, moviesArray);
            // Step 3: Return the final JSON response
            res.status(201).json({ message: 'Movie created successfully', movieData });
        }
    }
    catch (error) {
        console.error('Error:', error);
        res.status(500).json({ message: 'Internal Server Error' });
    }
};
exports.createMovie = createMovie;
